import Data.List
import Data.Char (chr)
import qualified Data.Set as S

type FormalConcept = (S.Set Int, S.Set Int)
data FormalContext a b = FormalContext {
    objects' :: S.Set a,
    attributes' :: S.Set b,
    relations :: S.Set (a,b),
    matrix :: [[Int]]           -- Here we keep both relations + matrix for the MOMENT
} deriving (Show)

matrix1 :: [[Int]]
matrix1 = [ [1,1,0,0,0,0,1,0,0],
            [1,1,0,0,0,0,1,1,0],
            [1,1,1,0,0,0,1,1,0],
            [1,0,1,0,0,0,1,1,1],
            [1,1,0,1,0,1,0,0,0],
            [1,1,1,1,0,1,0,0,0],
            [1,0,1,1,1,0,0,0,0],
            [1,0,1,1,0,1,0,0,0]]

buildContext :: [[Int]] -> FormalContext Int Int
buildContext m = FormalContext (S.fromList os) (S.fromList as) (S.fromList rs) m
    where os = [0..(length m - 1)] 
          as = [0..(length (head m) - 1)]
          rs = [(o,a) | o <- os, a <- as, (m !! o !! a) == 1]

context :: [[Bool]]
context = map (map (==1)) xs 
    where xs = [ [1,1,0,0,0,0,1,0,0],
                 [1,1,0,0,0,0,1,1,0],
                 [1,1,1,0,0,0,1,1,0],
                 [1,0,1,0,0,0,1,1,1],
                 [1,1,0,1,0,1,0,0,0],
                 [1,1,1,1,0,1,0,0,0],
                 [1,0,1,1,1,0,0,0,0],
                 [1,0,1,1,0,1,0,0,0]]

at :: Int -> Int -> Bool
at x y = context !! x !! y

at' :: Int -> Int -> Bool
at' = flip at

attributes :: S.Set Int
attributes = S.fromList [0..k]
    where k = (length $ head context) - 1

objects :: S.Set Int 
objects = S.fromList [0..(length context - 1)]

deriveIntent :: Int -> S.Set Int
deriveIntent y = S.filter (at' y) objects 

isCannonical :: Int -> S.Set Int -> S.Set Int -> Bool
isCannonical j extent intent = 
    null [k | k <- ks, k < j, all (at' k) is]
    where ks = S.toList $ attributes `S.difference` intent
          is = S.toList extent


inClose :: Int -> S.Set Int -> S.Set Int -> [FormalConcept] -> [FormalConcept]
inClose y extent intent xs
    | y == S.size attributes = (extent,intent):xs
    | size' == 0    = inClose (y+1) extent intent xs
    | size == size' = inClose (y+1) extent intent' xs
    | isCannonical y extent' intent = 
            inClose (y+1) extent intent temp
    | otherwise = inClose (y+1) extent intent xs
    where extent' = deriveIntent y `S.intersection` extent
          intent' = S.insert y intent
          size  = S.size extent
          size' = S.size extent'
          temp = inClose (y+1) extent' intent' xs

-- | Affichage rapide pour tester le resultat de inClose, lisible par un humain
temp :: [([Int],[Char])]
temp = toRealContext $ cleanInClose $ inClose 0 objects S.empty []

cleanInClose :: [FormalConcept] -> [([Int],[Int])]
cleanInClose = map (mapTuple S.toList)
    where mapTuple f (x,y) = (f x, f y)

toRealContext :: [([Int],[Int])] -> [([Int],[Char])]
toRealContext xs = map (map2Way (+1) chr') xs
   where chr' = chr.(+97)
         map2Way f g (x,y) =  (map f x, map g y)  


testResults :: [([Int], [Char])]
testResults = xs
    where xs = [
           ([1,2,3,4,5,6,7,8], "a"),
           ([1,2,3,5,6],     "ab"),
           ([3,6],         "abc"),
           ([6],       "abcdf"),
           ([3],       "abcgh"),
           ([5,6],       "abdf"),
           ([1,2,3],       "abg"),
           ([2,3],       "abgh"),
           ([3,4,6,7,8],     "ac"),
           ([6,7,8],       "acd"),
           ([7],         "acde"),
           ([6,8],       "acdf"),
           ([3,4],       "acgh"),
           ([4],       "acghi"),
           ([5,6,7,8],       "ad"),
           ([5,6,8],       "adf"),
           ([1,2,3,4],       "ag"),
           ([2,3,4],       "agh") ]

-- a=0, b=1, c=2, d=3, e=4, f=5, g=6, h=7, i=8
testInClose1 :: [([Int], [Char])]
testInClose1 = [x | x <- testResults, x `elem` temp]

