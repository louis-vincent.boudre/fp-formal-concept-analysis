#include <iostream>
#include <set>
#include <vector>

using namespace std;

#define M 8
#define N 9

using concepts = vector< pair< set<int>, set<int> > >;

bool IsCanonical(const set<int>&, const set<int>&, int);
concepts InClose(const int, const set<int>&, const set<int>&, const concepts&);

bool I[M][N] = {
    {1,1,0,0,0,0,1,0,0},
    {1,1,0,0,0,0,1,1,0},
    {1,1,1,0,0,0,1,1,0},
    {1,0,1,0,0,0,1,1,1},
    {1,1,0,1,0,1,0,0,0},
    {1,1,1,1,0,1,0,0,0},
    {1,0,1,1,1,0,0,0,0},
    {1,0,1,1,0,1,0,0,0}
}; 

//const set<int> D ({0,1,2,3,4,5,6,7,8});

int main() {

    set<int> A, B;
    for(int i = 0; i < N; i++) {
        A.insert(i);
    }
    
    concepts temp;
    concepts cs = InClose(0, A, B, temp);
    for(auto p : cs) {
        const set<int>& extent = p.first;
        const set<int>& intent = p.second;
         
        cout << "{";
        int j = 0;
        for(auto e : extent) {
            cout << (e+1);
            j++;
            if(j < extent.size()) {
                cout << ",";
            }
        }
        cout << "}, {";
        j = 0;
        for(auto x : intent) {
            cout << char(x+97);
            j++;
            if(j < intent.size()) {
                cout << ",";
            }
        }
        cout << "}" << endl;
    }
    
    cout << "Number of concepts: " << cs.size() << endl; 

    return 0;
}

set<int> attributes() 
{
    set<int> result;
    for(int i=0;i<N;++i) {
        result.insert(i);
    }
    return result;
}

set<int> objects()
{
    set<int> result;
    for(int i=0;i<M;++i) {
        result.insert(i);
    }
    return result;
}

set<int> set_intersection(const set<int>& s1, const set<int>& s2) 
{
    set<int> result;
    
    for(auto e : s1) {
        if(s2.find(e) != s2.end()){
            result.insert(e);
        }
    }

    return result;
}

set<int> set_difference(const set<int>& s1, const set<int>& s2) 
{
    set<int> result;
    for(auto e : s1) {
        if(s2.find(e) == s2.end()) {
            result.insert(e);
        }  
    }
    return result;
}

set<int> set_union(const set<int>& s1, const set<int>& s2) 
{
    set<int> result = s1;
    for(auto e : s2) {
        result.insert(e);
    }
    return result;
}

set<int> deriveIntent(int j) {
    set<int> result;
    for(int i = 0; i < M; i++) {
        if(I[i][j]) {
            result.insert(i);
        }
    }
    return result;
}

bool is_subset(const set<int>& s1, const set<int>& s2)
{
    for(auto e : s1) {
        if(s2.find(e) == s2.end()) 
            return false; 
    }

    return true;
}

bool isCanonical2(const set<int>& intent, const set<int>& extent, int y) 
{
    auto ks = set_difference(attributes(), intent);

    for(auto k : ks) {
        if(k > y) break;
        auto i = extent.begin();
        for(; i != extent.end(); ++i) {
            if(!I[*i][k]) break;
        }
        if(i == extent.end()) {
            return false;
        }
    }
    return true;
}

concepts InClose(const int j, 
            const set<int>& extent, 
            const set<int>& intent,
            const concepts& cs) 
{
    if(j == N) { 
        concepts temp = cs;
        temp.push_back(make_pair(extent,intent));
        return temp;
    }
    
    // extent2 and intent2 would be in a where clause in haskell
    auto extent2 = set_intersection(extent, deriveIntent(j));
    auto intent2 = intent; intent2.insert(j);
    
    if(extent2.size() == 0) {
        return InClose(j+1, extent, intent,cs);
    }
    if(extent2.size() == extent.size()) {
        return InClose(j+1, extent, intent2,cs);
    }
    if(isCanonical2(intent, extent2, j-1)) {
            concepts temp = InClose(j+1, extent2, intent2, cs);
            return InClose(j+1, extent,intent, temp);
    }
    return InClose(j+1, extent, intent, cs);
}



/**
 * B = current intent,
 * A = resulting extent,
 * y = B[y] = j = the new attribute.
 */ 
bool IsCanonical(const set<int>& B, const set<int>& A, int y) 
{
    set<int>::reverse_iterator k;
    for(k=B.rbegin(); k != B.rend(); ++k) {
        for(int j=y; j > *k; --j) {
            set<int> temp = deriveIntent(j); 
            if(is_subset(A, temp)) {
                return false;
            }
        }
        y = (*k)-1;
    }
    for(int j=y; j >= 0; --j) {
        set<int> temp = deriveIntent(j);
        if(is_subset(A, temp)) {
            return false;
        }
    }
    return true;
}
